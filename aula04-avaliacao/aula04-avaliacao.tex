\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Avaliação dos resultados}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today}
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Componentes do sistema de IR}
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/ir-query}
		\label{fig:ir-query}
		\caption{Componentes de um sistema de Recuperação da Informação \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Componentes detalhados}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-components}
		\label{fig:ir-components}
		\caption{Componentes em detalhes \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Componente: indexador}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-evaluation}
		\label{fig:ir-indexer}
		\caption{Quão bons são os resultados obtidos? \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Introdução}

\begin{frame}
    \frametitle{Máquinas de busca}
    \begin{itemize}
        \item Como medir a eficiência de uma máquina de busca?
        \item Qual a velocidade da indexação?
        \begin{itemize}
            \item Ex.: número de bytes por hora
        \end{itemize}
        \item Qual a velocidade da busca?
        \begin{itemize}
            \item Ex.: latência como uma função de consultas por segundo
        \end{itemize}
        \item Qual o custo por consulta?
        \begin{itemize}
            \item Ex.: em dólares
            \item Na área de Big Data essa medida é \alert{extremamente relevante}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Medidas subjetivas}
    \begin{itemize}
        \item Todos os critérios anteriores são \alert{mensuráveis}: é possível quantificar velocidade/tamanho/custo;
        \item Contudo, como saber se a busca deixou o \alert{usuário feliz};
        \item O que é felicidade para o usuário?
        \begin{itemize}
            \item Velocidade de resposta;
            \item Tamanho do índice;
            \item Interface amigável;
            \item Mais importante: \alert{relevância}.
        \end{itemize}
        \item Nenhuma dessa medidas é suficiente: não adianta retornar os resultados rapidamente se as respostas não forem úteis.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Quem é o usuário?}
    \begin{description}
        \item[Motor de busca na web -- usuário]  Quem está buscando conteúdo
        \begin{description}
            \item[Sucesso] O usuário encontrou o que estava buscando
            \item[Medida] Taxa de retorno ao motor de busca
        \end{description}
        \item[Motor de busca na web -- anunciante] Publicando anúncio
        \begin{description}
            \item[Sucesso] O usuário clica no anúncio
            \item[Medida] Quantidade de cliques (o famoso CPM)
        \end{description}
        \item[Ecommerce -- comprador] Buscando produtos
        \begin{description}
            \item[Sucesso] Comprador encontra o produto e compra
            \item[Medida] Taxa de conversão
        \end{description}
        \item[Ecommerce -- vendedor] Vendendo produtos
        \begin{description}
            \item[Sucesso] Vendedor consegue vender
            \item[Medida] Lucro por venda/itens vendidos
        \end{description}
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Horas de consumo}
    \begin{center}
        O duelo agora é pelo \alert{tempo de atenção das pessoas} por dia
    \end{center}
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/horas-netflix}
		\label{fig:horas-netflix}
		\caption{Notícia sobre horas assistindo Netflix}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Problemas reais}
    \begin{center}
        É um grande desafio encontrar o conteúdo buscado em vídeo
    \end{center}
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/globoplay-busca}
		\label{fig:globoplay-busca}
		\caption{Busca no globoplay}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Relevância}
    \begin{itemize}
        \item A definição mais comum para a felicidade do usuário relacionada à busca: \alert{relevância};
        \item Como se mede relevância?
        \item A metodologia padrão em recuperação de informação é baseada em três elementos \cite{cambridge}:
        \begin{enumerate}
            \item Uma coleção de documentos para \emph{benchmark};
            \item Um conjunto de consultas para \emph{benchmark};
            \item Uma ideia da relevância para conjuntos de documentos-consulta.
        \end{enumerate}
        \item O \emph{benchmark} é um referencial para avaliar os resultados.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Consulta e necessidade de informação}
    \begin{itemize}
        \item A relevância deve ser medida para quem? A consulta?
        \begin{exampleblock}{Necessidade de informação $i$}
            ``Estou procurando informação para saber se beber vinho tinto é mais efetivo para diminuir o risco de ataque cardíaco do que beber vinho branco''
        \end{exampleblock}
        \item Pode ser traduzida em
        \begin{exampleblock}{Consulta $q$}
            [vinho tinto vinho branco ataque cardíaco]
        \end{exampleblock}
        \item E o documento?
        \begin{exampleblock}{Documento $d'$}
            \begin{quote}
                No centro do discurso estava um ataque ao lobby da indústria de vinhos para diminuir o papel do vinho, tanto branco quanto tinto, nos acidentes relacionados a álcool e direção.
            \end{quote}
        \end{exampleblock}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Consulta x Informação}
    \begin{itemize}
         \item No exemplo apresentado, $d'$ é um excelente resultado para a consulta $q$, mas não é a informação procurada
         \item Nem sempre casar o resultado com a consulta(\alert{match}) significa encontrar a \alert{informação} desejada
         \item O usuário só vai ficar feliz se encontrar a informação, não se tiver um bom \emph{match} para a consulta realizada
         \item A literatura não ajuda: julgamento de relevância em relação à consulta é muito frequente, ainda que não represente a necessidade de informação do usuário
    \end{itemize}
\end{frame}


\section{Avaliação sem classificação}

\begin{frame}
    \frametitle{Precision e Recall}
    \begin{itemize}
        \item Precision(P) é a fração dos documentos recuperados que é relevante
        \[
            \begin{split}
	           Precision  &= \frac{\text{\#(documentos relevantes recuperados)}}{\text{\#(documentos recuperados)}} \\
	           Precision &= P(revelantes \mid recuperados)
            \end{split}
        \]
        \item Recall(R) é a fração dos documentos relevantes que são recuperados
        \[
            \begin{split}
	           Recall  &= \frac{\text{\#(documentos relevantes recuperados)}}{\text{\#(documentos relevantes)}} \\
	           Recall &= P(recuperados \mid relevantes)
            \end{split}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Como calcular}
    \begin{definition}
            As medidas Precision $P$ e Recall $R$ são definidas como:
            \[
                \begin{split}
                P &= \frac{TP}{TP + FP} \\
                R &= \frac{TP}{TP + FN}
                \end{split}
            \]
            onde TP e FN são definidos de acordo com a Tabela \ref{tab:precision-recall}.
        \begin{table}[ht]
            \centering
	        \begin{tabular}{c|c|c}
	             & Relevantes & Não relevantes \\
	             \hline
	             Recuperados & True Positives (TP) & Falsos Positives (FP) \\
	             \hline
	             Não recuperados & False Negatives (FN) & True Negatives (TN) \\
	        \end{tabular}
	        \label{tab:precision-recall}
	        \caption{Classificadores para positivos e negativos \cite{cambridge}}
	    \end{table}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Universo de documentos}
    \begin{itemize}
        \item Recall é uma função que não diminui de acordo com a quantidade de documentos retornados;
        \item Um sistema que retorna todos os documentos tem 100\% de Recall;
        \item Da mesma forma, retornar poucos documentos aumenta Precision.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Calculando F}
    \begin{definition}
        O operador $F$ pode ser definido como:
        \[
            \begin{split}
                F &= \frac{1}{\alpha \frac{1}{P} + (1 - \alpha)\frac{1}{R} } \\
                F &= \frac{(\beta^2 + 1)PR}{\beta^2 P + R}
            \end{split}
        \]
        onde 
        \[
            \beta^2 = \frac{1 - \alpha}{\alpha}
        \]
        e $\alpha \in [0, 1]$ e $\beta^2 \in [0, \infty]$ 
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{$F_1$ Score}
    \begin{itemize}
        \item O operador $F$ é mais utilizado no seu formato \alert{balanceado}:
        \begin{itemize}
            \item $\beta = 1$ ou
            \item $\alpha = 0.5$
        \end{itemize}
        \item O formato balanceado representa a \alert{média harmônica} entre P e R.
    \end{itemize}
    \[
        \begin{split}
            \frac{1}{F_{\alpha = 0.5}} &= \frac{1}{2}(\frac{1}{P} + \frac{1}{R}) \\
            F_{\beta = 1} &= \frac{2PR}{P+R}
        \end{split}
    \]
    \begin{itemize}
        \item O operador $F_{\beta = 1}$ também é conhecido como \alert{$F_1$ Score}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{table}
        \begin{tabular}{c|cc|c}
            & relevantes & não relevantes & total \\
            \hline
            recuperados & 20 & 40 & 60 \\
            não recuperados & 60 & 1.000.000 & 1.000.060 \\
            \hline
            & 80 & 1.000.040 & 1.000.120 \\
        \end{tabular}
        \label{tab:pr-example}
        \caption{Exemplo simples para cálculo de precision e recall \cite{cambridge}}
    \end{table}
    \begin{center}
        Utilizamos os dados da Tabela \ref{tab:pr-example} para calcular Precision e Recall:
        \[
            \begin{split}
                P &= \frac{20}{20+40} = \frac{1}{3} \\
                R &= \frac{20}{20+60} = \frac{1}{4} \\
                F_1 &= \frac{2*\frac{1}{3}*\frac{1}{4}}{\frac{1}{3} + \frac{1}{4}} = \frac{2}{7}
            \end{split}
        \]
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Acurácia}
    \begin{itemize}
        \item Por que utilizar medidas complexas como Precision, Recall, $F_1$?
        \item Por que não utilizar algo simples como a acurácia?
        \pause
        \item A acurácia mede a fração das decisões (relevante/irrelevante) que estão corretas.
        \item De acordo com a tabela \ref{tab:precision-recall}, podemos definir:
        \[
            accuracy = \frac{TP + TN}{TP + FP + FN + TN}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Problemas da acurácia}
    \begin{itemize}
        \item Truque simples para maximizar a acurácia: não retorne nenhum resultado;
        \item O objetivo de um sistema de RI é \alert{recuperar alguma informação} e possuir alguma tolerância em relação aos resultados incorretos;
        \item É aceitável retornar alguns resultados ruins, desde que alguma informação seja útil;
        \item As medidadas de Precision, Recall e $F_1$ são utilizadas para \alert{avaliação} dos algoritmos. Nunca para acurácia.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Criticidade}
    \begin{itemize}
        \item A relação inversa entre Precision e Recall força os sistemas a privilegiarem um em detrimento do outro;
        \item A melhor relação depende do \alert{caso de uso}.
    \end{itemize}
    \begin{table}[!ht]
        \begin{tabular}{|p{2.5cm}|p{3.5cm}|p{3.5cm}|}
            \hline
            \textbf{Variável} & \textbf{Foco em Precision} & \textbf{Foco em Recall} \\
            \hline
            Tempo & Importante & Menos importante \\
            \hline 
            Tolerância a perda de informações & Grande & Nenhuma \\
            \hline
            Redundância de informações & Pode haver muitas respostas igualmente satisfatórias & A informação normalmente está em um único documento \\
            \hline
            Exemplo & Propaganda contextual & Busca processual \\
            \hline
        \end{tabular}            
        \label{tab:pr-features}
        \caption{Análise sobre questões relativas ao foco em Precision ou Recall \cite{cambridge}}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Dificuldades}
    \begin{itemize}
        \item Para utilizar os valores de Precision, Recall e F é necessário testar um grande conjunto de buscas;
        \item É preciso julgar \alert{individualmente} cada par de consulta-documento;
        \item Existem alternativas com algoritmos mais complexos.
    \end{itemize}
\end{frame}


\section{Avaliação classificada}

\begin{frame}
    \frametitle{Avaliação com classificação}
    \begin{itemize}
        \item Precision/Recall/F são medidas para resultados \alert{não classificados};
        \item É possível transformar facilmente os resultados em \alert{listas classificadas};
        \item Para isso, basta adicionar uma medida a cada um dos documentos retornados: top-1, top-2, top-3, etc...
        \item O procedimento é chamado de Precision/Recall com classificação;
        \item As estatísticas de classificação indicam a facilidade para encontrar os documentos na lista.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Precision/Recall na posição n}
    \begin{minipage}[ht]{0.28\textwidth}
        \begin{table}[ht]
            \begin{tabular}{|c|c|}
                \hline
                Rank & Doc \\
                \hline
                1 & $d_{12}$ \\
                2 & $d_{123}$ \\
                3 & \alert{$d4$} \\
                4 & $d_{57}$ \\
                5 & $d_{157}$ \\
                6 & $d_{222}$ \\
                7 & \alert{$d_{24}$} \\
                8 & $d_{26}$ \\
                9 & \alert{$d_{77}$} \\
                10 & $d_{90}$ \\
                \hline
            \end{tabular}
	        \label{tab:pr-docs}
	        \caption{Exemplo de resultados de busca \cite{cambridge}}
        \end{table}
    \end{minipage}
    \begin{minipage}{0.7\textwidth}
	    Considerando o corte na posição $n$ na lista de ocorrências classificada dos documentos, consideramos que $P@n$ representa Precision na posição $n$:
	    \[
	        \begin{split}
	            P@n: &P@3 = \frac{1}{3}, P@5 = \frac{1}{5}, P@8 = \frac{2}{8} \\            
	            R@n: &R@3 = \frac{1}{3}, R@5 = \frac{1}{3}, R@8 = \frac{2}{3}
	        \end{split}
	    \]
	    Observe que, no cálculo de Precision, o numerador e denominador variam, enquanto no Recall o denominador é constante. Isso acontece porque o \alert{número de documentos relevantes é constante}.    
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Curva de Precision e Recall}
    \begin{itemize}
        \item Cada ponto na curva da Figura \ref{fig:pr-curve} corresponde a um resultado nos top-k classificados (k = 1, 2, 3, 4, ...)
        \item A interpolação, em vermelho, leva considera o máximo nos pontos futuros;
        \item Raciocínio: o usuário vai continuar as buscas e Recall se Precision melhorarem ao longo do tempo.
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.35\textwidth]{../imagens/pr-curve}
		\label{fig:pr-curve}
		\caption{Gráfico de Precision e Recall \cite{ir-book}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Recall fixo}
    Outra ideia: medir Precision em pontos fixos de Recall
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/precision-at-recall}
		\label{fig:precision-at-recall}
		\caption{\cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{11-point graph}
    \begin{itemize}
        \item Aplicando uma interpolação aos dados do Gráfico da Figura \ref{fig:pr-curve}, obtemos a curva da Figura \ref{fig:11point-graph};
        \item Faça isso para cada uma das consultas no benchmark;
        \item Calcule a média das várias consultas.
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/11point-graph}
		\label{fig:11point-graph}
		\caption{\cite{ir-book}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Média 11-point}
    \begin{definition}
        A precisão média 11-point pode ser definida como:
        \[
            P_{11\_pt} = \frac{1}{11} \sum_{j=0}^{10} \frac{1}{N} \sum_{i=1}^{N} \tilde{P_i}(r_j)
        \]
        onde $\tilde{P_i}(r_j)$ é o valor de Precision no ponto $j$ de Recall da consulta $i$.
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Calculando 11-point}
    \begin{itemize}
        \item Defina 11 pontos fixos para valor de Recall $r_j = \frac{j}{10} : r_0 = 0, r_1 = 0,1 ... r_{10} = 1$;
        \item Para calcular $\tilde{P_i}(r_j)$, é possível utilizar $P_i(R = r_j)$ se um novo documento relevante for encontrado exatamente em $r_j$.
        \item Interpolação para os casos em que não há uma medida exata em $r_j$:
        \[
            \tilde{P_i}(r_j) = \begin{cases}
                max(r_j \leq r < r_{j+1})P_i(R = r), &\text{ se } \exists P_i(R = r) \\
                \tilde{P_i}(r_{j+1}), & \text{caso contrário}
            \end{cases}
        \]
        \item Perceba que $P_i(R = 1)$ sempre pode ser medido.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MAP -- Mean Average Precision}
    \begin{definition}
        \[
            MAP = \frac{1}{N} \sum_{j=1}^N \frac{1}{Q_j} \sum_{i=1}^{Q_j} P(doc_i)
        \]
        \begin{description}
            \item[$Q_j$] Documentos relevantes retornados para a consulta $j$;
            \item[$N$] Número de consultas
            \item[$P(doc_i)$] Precision no documento relevante $i$
        \end{description}
    \end{definition}
    \begin{itemize}
        \item Precision média nos documentos retornados pela consulta;
        \item Determine a precisão em cada ponto quando cada novo documento é retornado;
        \item Utilize P = 0 para cada documento relevante que não for recuperado;
        \item Determine a média para cada uma das consultas;
        \item Calcule a média das consultas.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{MAP -- Exemplo}
    Para o exemplo da Figura \ref{fig:map-example}, calculamos a média das duas consultas:
    \[
        MAP = \frac{0.564 + 0.623}{2} = 0.594
    \]
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/map-example}
		\label{fig:map-example}
		\caption{Exemplo de cálculo do MAP para duas consultas \emph{Query 1} e \emph{Query 2} \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{R-precision}
    \begin{itemize}
        \item Para motores de busca na Internet (\emph{Web Search}) normalmente só importam os primeiros 10-20 resultados;
        \item Vantagem: não é necessário estimar o tamanho da coleção de documentos;
        \item Desvantagem: medida muito instável (média varia muito).
        \item Alternativa: \emph{R-precision}.
        \begin{itemize}
            \item Dada uma coleção de documentos relevantes $Rel$;
            \item \emph{R-precision} se ajusta à coleção de documentos: um algoritmo perfeito marcaria um score 1 nessa métrica;
            \item Novas medidas: se $r$ é o total de documentos relevantes, tanto Precision como Recall serão calculados da mesma forma: $\frac{r}{|Rel|}$
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Curva ROC}
    \begin{itemize}
        \item eixo-x: FPR (\emph{False Positive Rate}): FP/total de negativos;
        \item eixo-y: TPR (\emph{True Positive Rate}): TP/Total de positivos também chamado de \alert{sensitivity} $\equiv$ Recall;
        \item FPR = \emph{fall-out} = 1 - \alert{specificity} (TNR; \emph{True Negative Rate})
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/roc-curve}
		\label{fig:roc-curve}
		\caption{Curva ROC considerando os dados de Precision e Recall da Figura \ref{fig:pr-curve} \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Alternância de valores}
    \begin{itemize}
        \item Para uma coleção de testes, é comum um sistema que se comporta bem com uma necessidade de informação (ex.: P = 0,2 em R = 0,1) e muito bem em outras (ex.: R = 0,95 e R = 0,1)
        \item De fato, a \alert{variabilidade do mesmo sistema entre consultas}  é \alert{muito maior que a variância de diferentes sistemas na mesma consulta}.
        \item Resumo: existem informações que são \alert{fáceis de obter} e outras que são \alert{muito difíceis}.
        \item Para comparar os diferentes sistemas e/ou consultas precisaremos realizar \alert{testes comparativos} ou \alert{benchmarks}.
    \end{itemize}
\end{frame}

\section{Benchmark}

\begin{frame}
    \frametitle{Benchmark}
    \begin{itemize}
        \item Necessidades básicas para um benchmark:
        \begin{itemize}
            \item Uma coleção de documentos;
            \item Os documentos devem ser um substrato do dataset original.
        \end{itemize}
        \item Necessidades de informação:
        \begin{itemize}
            \item Normalmente vamos nos referenciar a elas (de maneira incorreta) como \alert{consultas};
            \item As necessidades de informação devem ser muito próximas das necessidades reais.
        \end{itemize}
        \item Avaliação \alert{humana} sobre a relevância:
        \begin{itemize}
            \item É preciso pagar/contratar avaliadores para julgar o resultado das consultas;
            \item Caro e consome muito tempo;
            \item Os avaliadores devem ser representantes dos usuários que vão utilizar as consultas;
            \item Se trata do grande \alert{desafio em aberto} em RI.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Cranfield}
    \begin{itemize}
        \item Primeiro método de testes com medidas quantitativas sobre a efetividade da recuperação da informação
        \item Final dos anos 50 no Reino Unido;
        \item Foram selecionados 1398 \emph{abstracts} de artigos sobre aerodinâmica para avaliação de 225 consultas;
        \item Vários juízes analisaram exaustivamente todas as consultas;
        \item Impraticável para um sistema de RI moderno.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{TREC}
    \begin{itemize}
        \item Técnica aplicada na TREC -- \emph{Text Retrieval Conference}
        \item Consiste em um conjunto de diferentes benchmarks;
        \item Mais conhecido: TREC Ad Hoc, utilizado entre 1992 e 1999;
        \item 1,89 minlhões de documentos, principalmente notícias, 450 necessidades de informação;
        \item Julgamento superficial da relevância (custo)
        \item Os melhor avaliados eram levados a um júri composto por especialistas em RI.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de consulta TREC \cite{cambridge}}
    <num> Número: 508 \\
    <title> queda de cabelo é sintoma para quais doenças \\
    <desc> Descrição: \\
    Encontrar doenças para as quais queda de cabelo é um sintoma \\
    <narr> Narrativa: \\
    Um documento é relevante se é capaz de conectar a queda de cabelos em seres humanos a uma doença específica. Nesse contexto, ``perda de cabelo'' e ``queda de cabelo'' são sinônimos. Perda de pelos faciais/corporais é irrelevante, assim como queda de cabela causada por utilização de medicamentos.
\end{frame}

\begin{frame}
    \frametitle{Processo de avaliação no TREC}
    \begin{figure}
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/trec}
		\label{fig:roc-curve}
		\caption{Mesas de avaliação no TREC \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{ClueWeb09}
    \begin{itemize}
        \item Avaliação de sistemas de RI na Web
        \item 1 bilhão de páginas Web;
        \item 25 terabytes ;
        \item Coletados entre Janeiro/Fevereiro 2009
        \item 10 idiomas
        \item 4.780.950.903 URL's únicas
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Concordância entre avaliadores}
    \begin{itemize}
        \item Os avaliadores discordam muito. O que é \alert{relevância} pra você?
        \item Apesar disso, a avaliação possui enorme impacto nos números de performance absolutos;
        \item Ex.: comparar o algoritmo A com o algoritmo B:
        \begin{itemize}
            \item Um sistema de RI deve fornecer uma resposta confiável a essa pergunta;
            \item Mesmo que ocorra muita discordância entre os avaliadores.
        \end{itemize}
        \item Isso vale para todos os casos?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Big Data}
    \begin{itemize}
        \item É praticamente possível medir o impacto do Recall em grandes volumes de dados.
        \item A \url{globo.com} publica algumas dezenas, às vezes centenas, de matérias por dia. Vamos olhar individualmente cada uma?
        \item Sistemas de busca normalmente utilizam Precision nos top k resultados;
        \item Algumas outras medidas que são muito úteis:
		\begin{description}
		    \item[Clickthrough]	Qual foi o primeiro resultado que você clicou?
		    \begin{itemize}
		        \item Não é muito confiável se analisado individualmente. Quem já acessou a notícia, leu o título e descartou o resto?
		        \item Considerando a \alert{jornada do usuário}, pode ser confiável.
		    \end{itemize}
		    \item[Teste A/B] Comparativo entre duas opções
		\end{description}
    \end{itemize}
\end{frame}


\begin{frame}
    \frametitle{Teste A/B}
    \begin{itemize}
        \item Objetivo: testar uma inovação simples
        \item Pré-requisito: existe um volume significativo de dados e usuários para testar
        \item Procedimento:
        \begin{itemize}
            \item Separe os grupos em normal, experimento e controle;
            \item O grupo de experimento vai receber a inovação;
            \item O resultado desse grupo vai ser comparado com o resultado dos usuários normais;
            \item Caso tenha alteração no resultado, compara-se com o grupo de controle para detectar anomalias;
            \item Caso passe na validação, o experimento de fato revelou que a inovação é uma boa hipótese.
        \end{itemize}
        \item Provavelmente o mais utilizado no Mercado.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Resumo}
    \begin{itemize}
        \item Foco em avaliação para recuperação de informação ad-hoc
        \begin{itemize}
            \item Precision, Recall, $F_1$ Score
            \item Mais medidas complexas para avaliação de dados classificados
        \end{itemize}
        \item Avaliação de tarefas \alert{interativas}
        \item Testar a \alert{significância} é um problema:
        \begin{itemize}
            \item Um bom resultado pode ter acontecido por acaso?
            \item O resultado se mantém ao utilizar outros conjuntos de documentos?
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../recuperacao-informacao}
\bibliographystyle{apalike}

\end{document}

