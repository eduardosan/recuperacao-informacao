\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Estruturas de Dados e Algoritmos para Indexação}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today}
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Componentes do sistema de IR}
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/ir-query}
		\label{fig:ir-query}
		\caption{Componentes de um sistema de Recuperação da Informação \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Componentes detalhados}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-components}
		\label{fig:ir-components}
		\caption{Componentes em detalhes \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Componente: indexador}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-indexer}
		\label{fig:ir-indexer}
		\caption{Foco: o indexador \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\section{Indexação}

\begin{frame}
    \frametitle{Índice Invertido}
    \begin{definition}
        Índice invertido é uma estrutura de dados construída para recuperar a informação de maneira eficiente. \cite{mannheim}
    \end{definition}
    \begin{itemize}
        \item O índice invertido contém uma lista de referências aos documentos para cada um dos termos:
        \begin{itemize}
            \item Para cada termo $t$ armazenamos a lista de todos os documentos que contém $t$;
            \item Os documentos são representados pelos seus identificadores.
        \end{itemize}
        \item A lista de documentos que contém o termo é chamada de \emph{posting list}, ou simplesmente \emph{posting};
        \item Em geral é implementada utilizando listas encadeadas.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Construção do índice invertido}
    \begin{itemize}
        \item Os principais passos para construção do índice invertido são \cite{cambridge}
        \begin{enumerate}
            \item Coletar os documentos que serão indexados;
            \item Tokenizar o texto;
            \item Realizar o processamento linguístico dos tokens;
            \item Indexar os documentos onde cada um dos termos aparece.
        \end{enumerate}
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/inverted-index}
		\label{fig:inverted-index}
		\caption{Exemplo de índice invertido \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definições}
    \begin{description}
        \item[Palavra] Conjunto contíguo de caracteres que aparece no texto.
        \item[Termo] Uma palavra "normalizada" (minúscula, morfologia, fonema, etc); uma classe de palavras equivalentes.
        \item[Token] Uma instância da palavra ou termo que aparece no documento.
        \item[Tipo] Uma classe de tokens equivalentes (na maior parte dos casos o mesmo que "termo")
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Ordenação}
    \begin{figure}
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/index-sort}
		\label{fig:index-sort}
		\caption{Criação de índice através de ordenação de palavras \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Agrupamento}
    \begin{minipage}[ht]{0.49\textwidth}
		\begin{center}
			\begin{figure}
				\includegraphics[width=.6\textwidth]{../imagens/index-grouping}
				\label{fig:index-grouping}
				\caption{Agrupamento por termos \cite{cambridge}}
			\end{figure}
		\end{center}
	\end{minipage}
	\begin{minipage}[ht]{0.49\textwidth}
	    \begin{itemize}
	        \item Primeiro ordenamos pelo termo para construir o \alert{dicionário};
	        \item Depois ordenamos pelo ID do documento (dentro da \emph{posting list});
	        \item Frequência no documento ($df$) = tamanho da lista de ID de documentos (\emph{posting list})
	        \begin{itemize}
	            \item Facilita a \alert{busca booleana} (Aula 01)
	            \item Auxilia o cálculo do do \alert{peso} (Aula 03)
	        \end{itemize}
	        \item Parte na memória, parte no disco.
	    \end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Skip lists}
    \begin{itemize}
        \item Algumas \emph{posting lists} podem conter milhões de entradas;
        \item Percorrer a lista individualmente para encontrar o documento pode ter custo elevado;
        \item Utilização de ponteiros de pulo (\emph{skip lists});
        \item Uma otimização interessante, mas que deixa de ser relevante com o poder computacional atual.
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/skip-lists}
		\label{fig:skip-lists}
		\caption{Exemplo de \emph{skip lists} \cite{cambridge}}
	\end{figure}
\end{frame}

\section{Normalização}

\begin{frame}
    \frametitle{Normalização de termos e documentos}
    \begin{center}
        Para construir um índice invertido é necessário obter do texto:
    \end{center}
    \begin{example}{}
        Faz frio na cidade de São Paulo. De novo...
    \end{example}
    \begin{center}
        A saída:
    \end{center}
    \begin{example}{}
        faz, frio, na, cidade, de, são, paulo, (...)
    \end{example}
    \begin{itemize}
        \item Cada token é um candidato para uma entrada na \emph{posting list}.
        \item Quais tokens são válidos e devem ser considerados?
    \end{itemize}
\end{frame}

\subsection{Documentos}

\begin{frame}
    \frametitle{Parser}
    \begin{itemize}
        \item Até o momento assumimos algumas premissas:
        \begin{itemize}
            \item Sabemos o que é um documento;
            \item O documento é facilmente lido e reconhecido por uma máquina.
        \end{itemize}
        \item É preciso lidar com o formato e o idioma de cada um dos documentos:
        \begin{itemize}
            \item O formato pode ser Excel, HTML, etc...
            \item O documento pode estar comprimido ou em formato binário (Excel, Word);
            \item Diferenças de \alert{codificação} (UTF-8, ISO-8859-1)
            \item Idioma pode ser Espanhol, Inglês, Portugês, etc...
        \end{itemize}
        \item Cada uma das características traz um problema estatístico diferente para classificação;
        \item Também é possível aplicar heurísticas.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Complicações}
    \begin{itemize}
        \item O mesmo índice pode conter termos em diversos idiomas;
        \item Documentos ou seus componentes podem conter múltiplos idiomas;
        \item Que documento está sendo indexado?
        \begin{itemize}
            \item Um e-mail?
            \item Um arquivo?
            \item Um e-mail com 5 anexos?
            \item Uma conversa com vários e-mails?
        \end{itemize}
        \item Problemas de hierarquia em arquivos HTML/XML;
        \item Tente responder a pergunta: o que é um documento?
        \item Unidades menores tendem a aumentar \emph{precision}, mas diminuem o \emph{recall}.
    \end{itemize}
\end{frame}

\subsection{Termos}

\begin{frame}
    \frametitle{Normalização}
    \begin{itemize}
        \item É necessário normalizar as palavras da consulta e do texto indexado da mesma forma;
        \item Exemplo: buscar tanto \alert{U.S.A.} quanto \alert{USA};
        \item Normalmente definimos implicitamente \alert{classes de equivalência} para termos;
        \item Alternativamente, é possível realizar uma expansão assimétrica:
    \end{itemize}
    \begin{example}{}
        \alert{janela} -> janelas \\
        \alert{Janelas} -> Janela, janelas, janela \\
        \alert{Janelas} -> Janelas
    \end{example}
    \begin{itemize}
        \item Mais poderoso, menos eficiente.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Tokenização}
    \begin{example}{}
        Saí de \alert{guarda-chuva}, mas não caiu uma gota \alert{d'água}.
    \end{example}
    \bigskip
	\begin{minipage}[ht]{0.49\textwidth}
	    \begin{itemize}
	        \item guarda-chuva
	        \item guarda-, chuva
	        \item guarda, -chuva
	        \item guarda, chuva
	        \item chuva
	        \item guarda
	    \end{itemize}
	\end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
        \begin{itemize}
            \item d'água
            \item d', água
            \item de, água
            \item d, água
            \item dágua
        \end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Problemas}
    \begin{center}
        Uma palavra, duas ou várias?
    \end{center}
    \begin{example}{}
        Hewlett-Packard \\
        guarda-chuva \\
        data base \\
        São Paulo \\
        Rio de Janeiro \\
        Ponte Rio-São Paulo
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Números}
	\begin{minipage}[ht]{0.49\textwidth}
        \begin{exampleblock}{}
	        \begin{center}
	            20/3/91 \\
	            20/03/1991 \\
	            20 de Março de 1991
	        \end{center}
	    \end{exampleblock}
        \begin{exampleblock}{}
	        \begin{center}
	            B-52 
	        \end{center}
        \end{exampleblock}
        \begin{exampleblock}{}
	        \begin{center}
	            6 anos de idade
	        \end{center}
        \end{exampleblock}
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
        \begin{exampleblock}{}
	        \begin{center}
	            100.2.86.144
	        \end{center}
	    \end{exampleblock}
        \begin{exampleblock}{}
	        \begin{center}
	            (61) 3966-1201 \\
	            61 3966 1201 \\
	            61.3966.1201
	        \end{center}
	    \end{exampleblock}
        \begin{exampleblock}{}
	        \begin{center}
	            .74189359872398457
	        \end{center}
	    \end{exampleblock}    
	\end{minipage}
	\begin{center}
	    Sistemas de RI antigos não costumam indexar números, mas pode ser muito útil.
	\end{center}
\end{frame}

\begin{frame}
    \frametitle{Outros idiomas}
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/chines}
		\label{fig:chines}
		\caption{Em chinês as palavras não possuem espaço como delimitador \cite{cambridge}}
	\end{figure}
	\begin{itemize}
	    \item Necessidade de segmentação de palavras;
	    \item Utilização de \emph{lexicon} ou algoritmos de aprendizagem supervisionada;
	    \item Ambiguidade:
	    \begin{figure}
	        \centering
			\includegraphics[width=.1\textwidth]{../imagens/chines-monk}
			\label{fig:chines-monk}
			\caption{Como uma palavra, significa "monge". Como duas palavras, significa "e" e "ainda". \cite{cambridge}}
		\end{figure}
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Alfabetos}
    \begin{itemize}
        \item Diferentes alfabetos podem estar misturados em um idioma;
        \item O japonês possui quatro alfabetos: kanji, katakana, hiragana, romanji
        \item Também não utiliza espaços.
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/japones}
		\label{fig:japones}
		\caption{Texto em japonês. \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Direção}
	\begin{itemize}
	    \item Alfabetos podem ser lidos em diferentes direções ao mesmo tempo;
	    \item O árabe mistura duas direções de leitura;
	    \item Ordem visual versus ordem conceitual.
	\end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/arabe}
		\label{fig:arabe}
		\caption{Texto em árabe. \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Termos compostos}
    \begin{center}
        Termos compostos em Alemão, Holandês e Sueco.
    \end{center}
    \begin{exampleblock}{Alemão}
        Lebensversicherungsgesellschaftsangestellter \\
        leben+\alert{s}+versicherung+\alert{s}+gesellschaft+\alert{s}+angestellter
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Aglutinação}
    \begin{center}
        Linguagens ``aglutinativas'' não utilizam apenas composição de termos:
    \end{center}
    \begin{exampleblock}{Inuit}
        tusaatsiarunnangittualuujunga \\
        (= ``I can’t hear very well'')
    \end{exampleblock}
    \begin{exampleblock}{Finlandês}
        epäjärjestelmällistyttämättömyydellänsäkäänköhän \\
        (= ``I wonder if – even with his/her quality of not
having been made unsystematized'')
    \end{exampleblock}
    \begin{exampleblock}{Turco}
        Çekoslovakyalılaştıramadıklarımızdanmşçasına \\
        (= ``as if you were one of those whom we could not
make resemble the Czechoslovacian people'')
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Maiúsculas e minúsculas}
    \begin{itemize}
        \item Alterar uma letra de minúscula para maiúscula pode alterar totalmente o significado da palavra:
    \end{itemize}
    \begin{exampleblock}{Inglês}
        Fed vs. fed \\
		March vs. march \\
		Turkey vs. turkey \\
		US vs. us
    \end{exampleblock}
    \begin{itemize}
        \item Na maior parte dos casos maiúsculas e minúsculas têm o mesmo significado.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Acentos e cedilhas}
    \begin{itemize}
        \item Acentos e cedilhas podem ser diferenciais semânticos:
    \end{itemize}
    \begin{exampleblock}{Espanhol}
        \alert{peña} = cliff, \alert{pena} = sorrow
    \end{exampleblock}
    \begin{itemize}
	    \item Normalmente o acento não afeta tanto o significado (résumé vs. resume)
	    \item A maior parte dos sistemas reduz todas as letras para minúsculas e removem os acentos e cedilhas.
	    \item E os usuários? Quando buscarem vão fazer o mesmo?        
    \end{itemize}    
\end{frame}

\begin{frame}
    \frametitle{Stop Words}
    \begin{itemize}
        \item Palavras muito comuns, mas que não auxiliam tanto o usuário em suas buscas.
    \end{itemize}
    \begin{exampleblock}{}
    'a',
 'ao',
 'aos',
 'aquela',
 'aquelas',
 'aquele',
 'aqueles',
 'aquilo',
 'as',
 'até',
 'com',
 'como',
 'da',
 'das',
 'de',
 'dela',
 'delas',
 'dele',
 'deles',
 'depois',
 'do',
 'dos',
 'e',
 'ela',
 'ser', 'estar'
    \end{exampleblock}
    \begin{itemize}
        \item Sistemas de RI antigos não costumavam indexar essas palavras
        \item Algumas buscas podem necessitar deles:
    \end{itemize}
    \begin{exampleblock}{}
        príncipe \alert{da} Dinamarca \\
        \alert{ser} ou não \alert{ser} \\
        namorado \alert{dela} 
    \end{exampleblock}
    \begin{itemize}
        \item A quantidade de \emph{stopwords} vem diminuindo
        \item Os motores de busca \alert{indexam} as \emph{stopwords}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lemmatização}
    \begin{definition}
        Reduzir as variantes de um termo para um formato base. \cite{cambridge}
    \end{definition}
    \begin{itemize}
        \item A Lemmatização implica reduzir o termo à sua raiz (\alert{lemma}).
    \end{itemize}
    \begin{exampleblock}{}
        sou, é, somos -> \alert{ser} \\
        carro, carros, carreata -> \alert{carro} \\
        As cores dos carros são diferentes -> As cores dos \alert{carro} \alert{ser} diferentes
    \end{exampleblock}
    \begin{itemize}
        \item Morfologia inflexiva (corte)
        \item Morfologia derivativa (destruição)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Stemming}
    \begin{definition}
        Processos heurístico que \alert{remove o radical} das palavras para aplicar a lemmatização com conhecimento da língua.
    \end{definition}
    \begin{itemize}
        \item Por necessitar de conhecimento da língua, os \emph{stemmers} são diferentes para cada idioma;
        \item Depende do idioma, mas é rápido e não ocupa muito espaço;
        \item Normalmente utiliza ambas inflexiva e derivativa.
    \end{itemize}
    \begin{exampleblock}{}
	    \begin{center}
	        automático, automação, autômato -> \alert{automat}    
	    \end{center}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Vários stemmers}
    \begin{itemize}
        \item Existem muitos Stemmers, mas todos eles dependem do \alert{idioma}.
        \item Alguns fornecidos pelo NLTK, que vamos testar:
        \begin{itemize}
            \item Porter Stemmer - Inglês;
            \item Lancaster Stemmer - Inglês;
            \item Snowball Stemmer - Português.            
        \end{itemize}
        \item Em geral, os algoritmos de stemmer tentam encontrar alguma medida de similaridade das palavras para agrupar por raiz.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Efeitos do stemmer}
    \begin{center}
        A utilização de um Stemmer aumenta a eficiência das consultas?
    \end{center}
    \begin{exampleblock}{Consultas onde o Stemmer para o Inglês ajuda}
        tartan sweaters -> sweater, sweaters \\
        sightseeing tour san francisco -> tour, tours
    \end{exampleblock}
    \begin{exampleblock}{Consultas onde o Stemmer para Inglês atrapalha}
        operational research -> oper \\
        operating system -> oper \\
        operative dentistry -> oper \\
        oper = operates, operatives, operate, operation, operational, operative
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Outras classes de equivalência}
    \begin{description}
        \item[Thessaurus] Equivalência semântica. Ex.: carro = automóvel
        \item[Fonemas] Equivalência fonética. Ex.: Muller = Mueller
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{BERT}
    \begin{itemize}
        \item Os \alert{transformers} mudam a forma com a qual trabalhamos com texto;
        \item Não existe normalização;
        \item Conceito de embeddings no BERT.
    \end{itemize}
    \begin{figure}
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/bert-embeddings}
		\label{fig:bert-embeddings}
		\caption{BERT e seu modelo de embeddings \cite{devlin_bert_2019}}
	\end{figure}
\end{frame}


\section{Índices complexos}

\begin{frame}
    \frametitle{Frases como consulta}
    \begin{itemize}
        \item E se quisermos buscar pela frase \alert{[cambridge university]}?
        \item Esses não deveriam ser encontrados:
        \begin{itemize}
            \item \emph{``The Duke of Cambridge was received by the vice chancellor at the university of Cambridge''}
            \item \emph{``The Duke if Cambridge was welcomed by University of Cambridge''}
        \end{itemize}
        \item Esse deve ser encontrado:
        \begin{itemize}
            \item \emph{``Prince Williams begins agricultural course at Cambridge University''}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Busca por frases}
    \begin{itemize}
         \item Representam uma parcela significativa das buscas;
         \item Para os índices invertidos não é mais suficiente apenas armazenar o ID do documento na lista;
         \item Duas opções para estender o índice invertido:
         \begin{itemize}
             \item Índices compostos;
             \item Índices posicionais.
         \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Índice de duplas}
    \begin{itemize}
        \item Vamos indexar cada duas palavras consecutivas no texto como uma frase;
        \item Cada uma dessas duplas é um termo do vocabulário;
        \item Agora é possível buscar por duplas de palavras.
    \end{itemize}
    \begin{exampleblock}{Friends, Romans, Countrymen}
        Gera duas duplas: \\
        friends romans \\
        romans countrymen
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Frases mais longas}
    \begin{itemize}
        \item Uma frase mais longa como \alert{cambridge university west campus} pode ser representada como uma consulta booleana.
    \end{itemize}
    \begin{exampleblock}{}
	    \begin{center}
	        \alert{cambridge university} AND \alert{university west} AND \alert{west campus}
	    \end{center}
    \end{exampleblock}
    \begin{itemize}
        \item Depois será necessário filtrar os resultados para saber quais realmente possuem as quatro palavras.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Problemas}
    \begin{itemize}
        \item O índice de duplas raramente é usado;
        \item Traz muitos falsos positivos, como demonstrado acima;
        \item Como o termo do vocabulário é grande demais, o índice deixa de ser eficiente.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Índices posicionais}
    \begin{itemize}
        \item São uma alternativa melhor em relação aos índices de dupla;
        \item Posting lists num índice não posicional: cada \emph{posting} é apenas um ID;
        \item Posting lists num índice posicional: cada \emph{posting} é um ID e uma lista de posições.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{figure}
        \centering
		\includegraphics[width=.55\textwidth]{../imagens/index-positional}
		\label{fig:index-positional}
		\caption{Exemplo de consulta com índice posicional. \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{figure}
        \centering
		\includegraphics[width=.55\textwidth]{../imagens/positional01}
		\label{fig:positional01}
		\caption{Adicionamos agora a posição no documento onde o termo está presente. \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{figure}
        \centering
		\includegraphics[width=.4\textwidth]{../imagens/positional02}
		\label{fig:positional02}
		\caption{Nos termos marcados em vermelho, vemos que não há ocorrência de `to` e `be`na sequência no Documento 4. Assim, eles casam com a cláusula `not to be`. \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Resumo}
    \begin{itemize}
        \item Aprendemos as unidades básicas de sistemas de recuperação da informação: \alert{palavras} e \alert{documentos}.
        \item O que é um \alert{documento}? O que é um \alert{termo}?
        \item \alert{Tokenização}: como transformar texto puro em termos (ou tokens)?
        \item Índices mais complexos para busca por frase:
        \begin{itemize}
            \item Índice de duplas;
            \item Índices posicionais.
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../recuperacao-informacao}
\bibliographystyle{apalike}

\end{document}
