\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Técnicas de agrupamento (clustering)}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Centro Universitário de Brasília -- UniCEUB}

\date{\today}
\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Componentes do sistema de IR}
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/ir-query}
		\label{fig:ir-query}
		\caption{Componentes de um sistema de Recuperação da Informação \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Componentes detalhados}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-components}
		\label{fig:ir-components}
		\caption{Componentes em detalhes \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Agrupando os resultados}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/ir-clustering}
		\label{fig:ir-query}
		\caption{Agrupando os resultados \cite{cambridge}}
	\end{figure}
\end{frame}


\section{Introdução}

\begin{frame}
    \frametitle{Definições}
    \begin{definition}
        Agrupamento (\emph{clustering}) é o processo de agrupar um conjunto de documentos em grupos de documentos semelhantes.
    \end{definition}
    \begin{itemize}
        \item Dentro de um grupo (\emph{cluster}) os documentos devem ser similares;
        \item Documentos de \emph{clusters} diferentes não devem ser similares.
        \item Clustering é a forma mais comum de \alert{aprendizado não supervisionado};
        \item No modelo não supervisionado não existem dados classificados ou anotados.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Diferenças}
    \begin{table}[!ht]
        \begin{tabular}{|p{5cm}|p{5cm}|}
            \hline
            \textbf{Classificação} & \textbf{Clustering} \\
            \hline
            aprendizado supervisionado & aprendizado não supervisionado \\
            \hline
            classes são definidas por \alert{humanos} e também são parte da entrada para o algoritmo de aprendizagem & Clusters são \alert{inferidos dos dados} sem participação humana \\
            \hline
            saída = membro de apenas uma classe & saída = membro de uma classe + distância do centróide (grau de pertencimento ao cluster) \\
            \hline
        \end{tabular}
        \label{tab:cluster-classification}
        \caption{Diferenças entre clustering e classificação}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Hipótese do cluster}
    \begin{exampleblock}{Hipótese do Cluster}
        Documentos no mesmo cluster se comportam de maneira similar em relação à relevância para as necessidades de informação.
    \end{exampleblock}
    \begin{itemize}
        \item Todas as aplicações de clustering são baseadas em inteligência artificial (IR) na hipótese do cluster, direta ou indiretamente.
    \end{itemize}
    \begin{exampleblock}{}
        \begin{quote}
            ``Documentos próximos devem ser relevantes para as mesmas consultas''. \footnote{Palavras ditas por Van Rijsbergens em 1979 \cite{cambridge}}
        \end{quote}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Aplicações}
	\begin{description}
	    \item[IR] Apresentação dos resultados (clustering de documentos)
	    \item[Sumarização] Técnica para sumarizar os documentos
	    \begin{itemize}
	        \item Clustering de documentos similares para sumarização multi-documentos;
	        \item Clustering de sentenças similares para reescrever as sentenças.
	    \end{itemize}
	    \item[Segmentação de tópicos] Clustering de parágrafos similares (adjacentes ou não adjacentes) para detecção da estrutura tópico/importância;
	    \item[Semântica e léxica] Clustering de palavras para padrões de coocorrência.
	\end{description}
\end{frame}

\begin{frame}
    \frametitle{Cluster de resultados de busca}
    \begin{figure}
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/carrot}
		\label{fig:carrot}
		\caption{Cluster de resultados de busca utilizando a ferramenta Carrot\footnote{\url{https://search.carrotsearch.com/\#/search/pubmed/data\%20mining/folders}}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster de notícias}
    \begin{figure}
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/newsmap}
		\label{fig:newsmap}
		\caption{Cluster de notícias\footnote{\url{http://newsmap.jp/}}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster de tópicos}
    \begin{figure}
        \centering
		\includegraphics[width=.75\textwidth]{../imagens/clustering-of-news-sections}
		\label{fig:clustering-of-news-sections}
		\caption{Cluster de seções de notícias, agrupando em \emph{Hard News} e \emph{Soft News} \cite{cluster}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster de tópicos científicos}
    \begin{figure}
        \centering
		\includegraphics[width=.65\textwidth]{../imagens/topics-aag}
		\label{fig:topics-aag}
		\caption{Cluster de tópicos na Conferência AAG \cite{cambridge}}
	\end{figure}
\end{frame}


\begin{frame}
    \frametitle{Cluster de patentes}
    \begin{figure}
        \centering
		\includegraphics[width=.45\textwidth]{../imagens/patent-animals}
		\label{fig:patent-animals}
		\caption{Cluster de patentes relacionadas a animais\footnote{Análise disponível em \url{https://www.pauloldham.net/overview-open-tools/}}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster de termos}
    \begin{figure}[ht]
	    \begin{minipage}[ht]{0.48\textwidth}
	        \includegraphics[width=.9\textwidth]{../imagens/aecio}
	    \end{minipage}
	    \begin{minipage}[ht]{0.48\textwidth}
	        \includegraphics[width=.9\textwidth]{../imagens/dilma}
	    \end{minipage}
	    \label{fib:dilma-aecio}
	    \caption{Análise dos planos de governo de Dilma e Aécio em 2014\footnote{Disponível em \url{https://eduardosan.com/2014/10/22/o-que-escrevem-os-candidatos-a-presidente-em-2014/}}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{O estado da arte}
    \begin{figure}
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/weblyzard}
		\label{fig:weblyzard}
		\caption{E se fosse possível tudo ao mesmo tempo?\footnote{Análise disponível em \url{https://us2018.weblyzard.com/}}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Tipos de Clustering}
    \begin{itemize}
        \item Hard clustering x Soft Clustering
        \begin{description}
            \item[Hard] Cada objeto é membro de um cluster somente
            \item[Soft] Objetos podem ser membros de mais de um cluster
        \end{description}
        \item Clustering Hierárquico x Não hierárquico
        \begin{description}
            \item[Hierárquico] Os pares dos clusters mais similares são conectados interativamente a todos os objetos em uma relação de clustering
            \item[Não hierárquico] Agrupar os resultados em clusters horizontais de documentos similares
        \end{description}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Resumo sobre clustering}
    \begin{itemize}
        \item \alert{Objetivo geral}: colocar documentos relacionados no mesmo cluster, documentos não relacionados em clusters diferentes
        \item O \alert{número de clusters} deve ser apropriado para o dataset que está sendo clusterizado
        \begin{itemize}
            \item A premissa inicial é de que todos os clusters K são fornecidos
            \item Também existem métodos semi automáticos para determinar K
        \end{itemize}
        \item \alert{Objetivos secundários}:
        \begin{itemize}
            \item Evitar clusters muito pequenos e muito grandes
            \item Definir clusters que sejam fáceis de explicar para o usuário
            \item Objetivos \alert{de negócio}
        \end{itemize}
    \end{itemize}
\end{frame}

\section{Clustering não hierárquico}

\begin{frame}
    \frametitle{Cluster não hierárquico (parcial)}
    \begin{itemize}
        \item Algoritmos de clustering parcial produzem um conjunto de k partições não testados correspondentes aos k cluster de n objetos;\
        \item \alert{Vantagem}: não é necessário comparar cada objeto com os outros. A comparação é feita somente com os objetos mais próximos ao centro do cluster;         
        \item Algoritmos ótimos para particionamento possuem complexidade $O(kn)$;
        \item Principal algoritmo: k-means
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{K-means}
    \begin{itemize}
        \item Cada cluster $j$ (com $n_j$ elementos $x_i$) é representado pelo seu \alert{centroide} $c_j$, o vetor médio do cluster:
	    \[
	        c_j = \frac{1}{n_j}\sum_{i=1}^{n_j} x_i
	    \]
	    \item Medida de qualidade do cluster: minimizar a distância quadrada média entre os elementos $x_i$ e o centróide mais próximo $c_j$
	    \[
	        RSS = \sum_{j=1}^{k} \sum_{x_i \in j} d(\vec{x_i}, \vec{c_j})^2 \Rightarrow \text{função-objetivo}
	    \]
	    \item Distância \alert{euclidiana};
	    \item Vetores normalizados em relação ao tamanho no espaço vetorial.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{k-means: iteração}
    \begin{itemize}
        \item Para aplicar o método é necessário realizar duas iterações para cada elemento:
        \begin{enumerate}
            \item \alert{reatribuição}: atribuir cada vetor ao seu centroide mais próximo;
            \item \alert{recálculo}: recalcular cada centroide como a média dos vetores conforme novos elementos vão sendo adicionados.
        \end{enumerate}
        \item O número de iterações tem como objetivo minimizar o valor da função $RSS$, também conhecida como \alert{função-objetivo}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{O algoritmo k-means}
    \begin{figure}
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/k-means-algorithm}
		\label{fig:k-means-algorithm}
		\caption{Pseudocódigo para implementação do algoritmo k-means \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{enumerate}
        \item Adivinhe qual o clustering ótimo quando temos dois clusters;
        \item Calcule o centroide dos clusters
    \end{enumerate}
    \begin{figure}
        \centering
		\includegraphics[width=.5\textwidth]{../imagens/k-means01}
		\label{fig:k-means01}
		\caption{Dataset para teste do clustering por k-means \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 1}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means02}
		\label{fig:k-means02}
		\caption{Começamos com pontos aleatórios e atribuímos cada um deles ao centro mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 1}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means03}
		\label{fig:k-means03}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 1}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means04}
		\label{fig:k-means04}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 2}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means05}
		\label{fig:k-means05}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 2}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means06}
		\label{fig:k-means06}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 3}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means07}
		\label{fig:k-means07}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 3}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means08}
		\label{fig:k-means08}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 4}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means09}
		\label{fig:k-means09}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 4}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means10}
		\label{fig:k-means10}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 5}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means11}
		\label{fig:k-means11}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 5}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means12}
		\label{fig:k-means12}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 6}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means13}
		\label{fig:k-means13}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 6}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means14}
		\label{fig:k-means14}
		\caption{Atribuímos os pontos ao centroide mais próximo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Iteração 7}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means15}
		\label{fig:k-means15}
		\caption{Recalculamos o centroide do cluster \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo -- Convergência}
    \begin{figure}
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/k-means16}
		\label{fig:k-means16}
		\caption{Centroides e atribuições depois da convergência \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Prova de convergência}
    \begin{itemize}
        \item O algoritmo k-means \alert{sempre converge}:
        \begin{itemize}
	        \item O valor de $RSS$ diminui a cada passo de reatribuição: cada vetor é movido para o centroide mais próximo;
	        \item O valor de $RSS$ diminui a cada passo de recálculo: o novo centroide é o vetor para o qual o valor de $RSS_k$ é mínimo;
	        \item Existe um número finito de clusters;
	        \item Assim, é possível chegar a um ponto no espaço vetorial;
	        \item Conjunto finito de dados e função de decaimento monotônico $\Rightarrow$ convergência;            
	        \end{itemize}
	        \item \alert{Premissa}: existe um método consistente para o desempate.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Outras propriedades}
    \begin{itemize}
        \item \alert{Convergência rápida}
        \begin{itemize}
            \item O k-means normalmente atinge a convergência entre 10-20 iterações, desde que não seja relevante alguns documentos trocarem de cluster constantemente;
            \item Contudo, completar a convergência pode requerer mais iterações.
        \end{itemize}
        \item \alert{Não ótimo}
        \begin{itemize}
            \item O k-means não garante encontrar a solução ótima;
            \item Se o conjunto de pontos inicial escolhido for ruim, o agrupamento resultante pode ser péssimo.
        \end{itemize}
        \item \alert{Dependência de centroides iniciais}
        \begin{description}
            \item[Solução 1] Utilize $i$ grupos e selecione aquele com o menor $RSS$;
            \item[Solução 2] Utilize um método de clustering hierárquico antes para encontrar os pontos iniciais com melhor cobertura do espaço de documentos.
        \end{description}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Complexidade}
    \begin{itemize}
        \item Etapa de reatribuição: $O(KNM)$. É preciso calcular as $KN$ distâncias entre o documento e o centroide, sendo que cada uma custa $O(M)$
        \item Etapa de recálculo: $O(NM)$. É preciso adicionar cada um dos documentos cujo valor é menor que $M$ a um dos centroides
        \item Assuma que o número de iterações é limitado por $I$
        \item Complexidade total: $O(IKNM)$ -- linear em todas as dimensões importantes.
    \end{itemize}
\end{frame}

\section{Clustering hierárquico}

\begin{frame}
    \frametitle{Introdução}
    \begin{itemize}
        \item Imagine que vamos criar uma hierarquia na forma de uma árvore binaria;
        \item Assuma que existe uma medida para determinar a similaridade entre dois \alert{clusters};
        \item Até agora, nossas medidas de similaridade eram para \alert{documentos};
        \item Agora vamos considerar a similaridade entre dois diferentes clusters;
        \item Principal algoritmo: HAC -- \emph{Hierarchical agglomerative clustering}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{HAC -- Algoritmo básico}
    \begin{itemize}
        \item Comece com cada documento em um cluster separado;
        \item Depois \alert{una repetidamente} os dois clusters que são mais similares;
        \item Repita a operação até que haja somente um cluster;
        \item Ao unir os clusters está sendo construída uma hierarquia na forma de árvore binária;
        \item A forma correta de representar a união dos clusters é através de um \alert{dendrograma}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Dendrograma}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.55\textwidth]{../imagens/dendrogram}
		\label{fig:dendrogram}
		\caption{Exemplo de dendrograma \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Calculando}
    \begin{itemize}
        \item Aplicar a métrica de proximidade para todos os pares de documentos cria a matriz de documentos e documentos;
        \item A matriz apresenta a similaridade/distância entre objetos (documentos);
        \item Como as medidas de proximidade são simétricas, a matriz é um triângulo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Retomando o exemplo}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/dd-matrix}
		\label{fig:dd-matrix}
		\caption{Saímos da matriz de Termos e Documentos (TF-IDF) para a matriz de Documentos e Documentos \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster hierárquico}
    A função de similaridade $sim : \mathcal{P}(X) x \mathcal{P}(X) \rightarrow \mathcal{R}$ mede a similaridade entre \alert{clusters}, não objetos.
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.6\textwidth]{../imagens/cluster-hierarquico}
		\label{fig:cluster-hierarquico}
		\caption{Implementação do algoritmo de cluster hierárquico utilizado a abordagem gulosa \emph{Bottom-Up} \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complexidade}
    \begin{itemize}
        \item Primeiro computamos a complexidade de todos os pares de documentos $N x N$;
        \item Depois, para cada iteração:
        \begin{itemize}
            \item Verificamos as $O(N x N)$ similaridades para encontrar a maior;
            \item Juntamos os dois clusters com similaridade máxima;
            \item Computamos a similaridade do novo cluster com todos os outros (sobreviventes).
        \end{itemize}
        \item Existem $O(N)$ iterações, cada uma executando uma operação de ``busca'' $O(N x N)$;
        \item Complexidade total é $O(N^3)$
        \item Dependendo da função de similaridade, é possível encontrar um algoritmo mais eficiente.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Funções de similaridade}
    A similaridade entre dois clusters $c_k$ e $c_j$ (com medida de similaridade $s$) pode ser interpretada de formas diferentes:
    \begin{description}
        \item[Função single-link] Similaridade entre os dois membros mais similares
        \[
            sim(c_u, c_v) = max_{x \in c_u, y \in c_k}s(x, y)
        \]
        \item[Função complete-link] Similaridade de pelo menos dois membros
        \[
            sim(c_u, c_v) = min_{x \in c_u, y \in c_k}s(x, y)
        \]
        \item[Função group-average] Similaridade média de cada par de membros do grupo
        \[
            sim(c_u, c_v) = avg_{x \in c_u, y \in c_k}s(x, y)
        \]
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/ah-example}
		\label{fig:ah-example}
		\caption{Exemplo de cluster para 8 objetos (a-h). As distâncias euclidianas estão apresentadas no diagrama 2D \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Single Link}
    Observe que a complexidade é $O(n^2)$
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/single-link}
		\label{fig:single-link}
		\caption{Cálculo de similaridade utilizando single-link para o cluster de 8 objetos \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Single Link Cluster}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/single-link-graph}
		\label{fig:single-link-graph}
		\caption{Resultado do cluster utilizando a função single-link \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complete link}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/complete-link01}
		\label{fig:complete-link01}
		\caption{Cálculo de similaridade utilizando complete-link para o cluster de 8 objetos \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complete link}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/complete-link02}
		\label{fig:complete-link02}
		\caption{Agora calculamos max-min para cada passo \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complete link Cluster}
    A complexidade da função complete-link é $O(n^3)$
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.8\textwidth]{../imagens/complete-link-graph}
		\label{fig:complete-link-graph}
		\caption{Resultado do cluster utilizando a função complete-link  \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplo: genes}
    \begin{itemize}
        \item Um exemplo da biologia: agrupar os genes por \alert{função};
        \item Investigar 112 genes suspeitos de participar no desenvolvimento de CNS;
        \item Escolher 9 pontos: 5 embrionários (E11, E13, E15, E18, E21), 3 após o nascimento (P0, P7, P14) e um adulto;
        \item Medir a expressividade do gene: quanto mRNA na célula?
        \item As medidas são normalizadas por log; para o propósito do problema, podemos considerá-las como pesos;
        \item Análise do cluster determina quais genes agem ao mesmo tempo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Expressividade dos genes}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.9\textwidth]{../imagens/rat-cns}
		\label{fig:rat-cns}
		\caption{Dados de expressividade do gene CNS para ratos \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster CNS -- single-link}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/cns-clustering}
		\label{fig:cns-clustering}
		\caption{Exemplo de cluster CNS para os genes dos ratos utilizando single-link \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster CNS -- complete-link}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/cns-cluster-complete}
		\label{fig:cns-cluster-complete}
		\caption{Exemplo de cluster CNS para os genes dos ratos utilizando complete-link \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster CNS -- average-link}
    \begin{figure}[ht]
        \centering
		\includegraphics[width=.7\textwidth]{../imagens/cns-cluster-average}
		\label{fig:cns-cluster-average}
		\caption{Exemplo de cluster CNS para os genes dos ratos utilizando average-link \cite{cambridge}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cluster horizontal ou hierárquico?}
    \begin{itemize}
        \item Quando desejamos construir estrutura hierárquica, melhor aplicar um algoritmo hierárquico;
        \item Humanos são muito ruins em enxergar cluster, a não ser quando visualizados;
        \item Para grande eficiência, cluster horizontal;
        \item Para regras determinísticas, HAC;
        \item HAC também pode ser utilizado se o valor de K não puder ser determinado a priori.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Resumo}
    \begin{itemize}
        \item Clustering é aprendizado \alert{não supervisionado};
        \item Clustering parcial:
        \begin{itemize}
            \item Fornece menos informação, mais eficiente ($O(kn)$);
            \item k-means:
            \begin{itemize}
                \item Complexidade $O(knmi)$
                \item Certeza de convergência, não é ótimo, depende dos pontos iniciais.
            \end{itemize}
            \item Clustering hierárquico:
            \begin{itemize}
                \item Melhores algoritmos possuem complexidade $O(n^2)$;
                \item Single-link vs complete-link (vs. group-average)
            \end{itemize}
            \item Clustering hierárquico e não hierárquico possuem diferentes aplicações (visualização vs navegação)
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../recuperacao-informacao}
\bibliographystyle{apalike}

\end{document}

